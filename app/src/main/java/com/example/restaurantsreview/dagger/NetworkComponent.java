package com.example.restaurantsreview.dagger;

import com.example.restaurantsreview.activity.DashboardActivity;
import com.example.restaurantsreview.activity.SplashActivity;
import com.example.restaurantsreview.repository.RestaurantDetailsRepository;
import com.example.restaurantsreview.repository.RestaurantListRepository;
import com.example.restaurantsreview.repository.ReviewRepository;
import com.example.restaurantsreview.repository.SearchRepository;

import javax.inject.Singleton;

import dagger.Component;

@Component(modules = NetworkModule.class)
@Singleton
public interface NetworkComponent {

    void inject(SplashActivity splashActivity);
    void inject(DashboardActivity dashboardActivity);

    void inject(RestaurantListRepository restaurantListRepository);
    void inject(RestaurantDetailsRepository restaurantDetailsRepository);
    void inject(SearchRepository searchRepository);
    void inject(ReviewRepository reviewRepository);

}