package com.example.restaurantsreview.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import com.example.restaurantsreview.R;
import com.example.restaurantsreview.common.Analytics;
import com.example.restaurantsreview.view.SearchFragment;
import com.example.restaurantsreview.entities.search.Restaurant;
import timber.log.Timber;

public class SearchActivity extends AppCompatActivity implements SearchFragment.ISearchAcion {

    private static final String FTAG_SEARCH_FRAGMENT = "fragmentSearch";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        SearchFragment fragment = (SearchFragment) getSupportFragmentManager().findFragmentByTag(FTAG_SEARCH_FRAGMENT);
        if(fragment == null) {
            fragment = new SearchFragment();
            Bundle bundle = new Bundle();
            bundle.putParcelable("location", getIntent().getParcelableExtra("location"));
            fragment.setArguments(bundle);
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fl_search_activity, fragment, FTAG_SEARCH_FRAGMENT)
                    .commit();
        }
        fragment.setSearchAction(this);
        //Analytics information
        Analytics.setCurrentScreen(this, "SearchFragment Activity");
    }

    @Override
    public void onRestaurantSelected(Restaurant restaurant) {
        Timber.d(restaurant.getName());
        Intent launchIntent = new Intent(this, RestaurantDetailsActivity.class);
        launchIntent.putExtra("resid", restaurant.getId());
        launchIntent.putExtra("rName", restaurant.getName());
        launchIntent.putExtra("rCuisine", restaurant.getCuisines());
        startActivity(launchIntent);
    }
}