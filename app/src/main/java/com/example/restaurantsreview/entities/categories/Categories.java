package com.example.restaurantsreview.entities.categories;

import android.os.Parcel;
import android.os.Parcelable;

public class Categories implements Parcelable {

    public String name;
    public int id;

	protected Categories(Parcel in) {
		name = in.readString();
		id = in.readInt();
	}

	public static final Creator<Categories> CREATOR = new Creator<Categories>() {
		@Override
		public Categories createFromParcel(Parcel in) {
			return new Categories(in);
		}

		@Override
		public Categories[] newArray(int size) {
			return new Categories[size];
		}
	};

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel parcel, int i) {
		parcel.writeString(name);
		parcel.writeInt(id);
	}
}
