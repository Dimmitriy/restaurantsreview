package com.example.restaurantsreview.entities.categories;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class CategoryResponse implements Parcelable {

	public List<CategoriesItem> categories;

	protected CategoryResponse(Parcel in) {
		categories = in.createTypedArrayList(CategoriesItem.CREATOR);
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeTypedList(categories);
	}

	@Override
	public int describeContents() {
		return 0;
	}

	public static final Creator<CategoryResponse> CREATOR = new Creator<CategoryResponse>() {
		@Override
		public CategoryResponse createFromParcel(Parcel in) {
			return new CategoryResponse(in);
		}

		@Override
		public CategoryResponse[] newArray(int size) {
			return new CategoryResponse[size];
		}
	};

	public void setCategories(List<CategoriesItem> categories){
		this.categories = categories;
	}

	public List<CategoriesItem> getCategories(){
		return categories;
	}
}