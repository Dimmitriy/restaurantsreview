package com.example.restaurantsreview.entities.categories;

import android.os.Parcel;
import android.os.Parcelable;

public class CategoriesItem implements Parcelable {

	public Categories categories;

	protected CategoriesItem(Parcel in) {
		categories = in.readParcelable(Categories.class.getClassLoader());
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeParcelable(categories, flags);
	}

	@Override
	public int describeContents() {
		return 0;
	}

	public static final Creator<CategoriesItem> CREATOR = new Creator<CategoriesItem>() {
		@Override
		public CategoriesItem createFromParcel(Parcel in) {
			return new CategoriesItem(in);
		}

		@Override
		public CategoriesItem[] newArray(int size) {
			return new CategoriesItem[size];
		}
	};

	public void setCategories(Categories categories){
		this.categories = categories;
	}

	public Categories getCategories(){
		return categories;
	}
}
