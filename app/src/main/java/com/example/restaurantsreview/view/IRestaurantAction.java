package com.example.restaurantsreview.view;

import com.example.restaurantsreview.entities.search.Restaurant;

public interface IRestaurantAction {
    void onClick(Restaurant restaurant);
}