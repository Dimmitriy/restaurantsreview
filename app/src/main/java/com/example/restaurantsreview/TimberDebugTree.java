package com.example.restaurantsreview;

import timber.log.Timber;

public class TimberDebugTree extends Timber.DebugTree {

    @Override
    protected String createStackElementTag(StackTraceElement element) {
        return super.createStackElementTag(element) + "[" + element.getLineNumber() + "]";
    }
}
